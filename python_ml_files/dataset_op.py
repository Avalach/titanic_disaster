###################################################################################################
#                                                                                                 #
#     Dataset operations based on module panda                                                    #
#                                                                                                 #
###################################################################################################

import datetime

import numpy      as np
import pandas     as pd
import output     as outp
import general_op as gop



#--------------------------------------------------------------------------------------------------
# Reading  csv-file as panda dataframe:
#
# Input variables:
# file_name: type(str):       File name of the input csv-file
#
# Return variables:
# df:        type(dataframe): Dataframe of the csv-file
#

def read_csv_dataframe(file_name, **kwargs):

   if "df_kwargs" in kwargs:
      df = pd.read_csv(file_name, **kwargs["df_kwargs"])
   else:
      df = pd.read_csv(file_name)
   if "set_freq" in kwargs:
      df = df.asfreq(pd.infer_freq(df.index))
   return df



#--------------------------------------------------------------------------------------------------
# Writing panda dataframe as csv-file:
#
# Input variables:
# df       : type(dataframe): Dataframe  
# file_name: type(str):       File name of the output csv-file
#
def write_csv_dataframe(df, file_name):

   df.to_csv(file_name)



#--------------------------------------------------------------------------------------------------
# Get first row of a panda dataframe:
#
# Input variables:
# df:         type(dataframe): Dataframe
#
# Return variables:
# df.columns: type(list):      First row of the dataframe
#

def dataframe_columns(df):

   return df.columns



#--------------------------------------------------------------------------------------------------
# Get index column of a panda dataframe:
#
# Input variables:
# df:       type(dataframe): Dataframe
#
# Return variables:
# df.index: type(list):      First row of the dataframe
#

def dataframe_index(df):

   return df.index



#--------------------------------------------------------------------------------------------------
# Get size of a panda dataframe:
#
# Input variables:
# df:       type(dataframe): Dataframe
#
# Return variables:
# df.size: type(int):        Size of the dataframe (all data elements)
#

def dataframe_size(df):

   return df.size




#--------------------------------------------------------------------------------------------------
# Cut dataframe from top or bottom
#
# Input variables:
# df:      type(dataframe): Dataframe
# cut_dir: type(str):       Cut direction (top/bottom)
# cut_nr:  type(int):       Number of cutted rows
#
# Return variables:
# df_cut:  type(dataframe): Cutted dataframe
#

def cut_dataframe(df, cut_dir, cut_nr):

   if (cut_dir == "top"):
      df_cut = df.head(cut_nr)
   elif (cut_dir == "bottom"):
      df_cut = df.tail(cut_nr)
   else:
      outp.message_line("Wrong statement in position 2 (choose top or bottom)!",headword="Fatal error")
   return df_cut



#--------------------------------------------------------------------------------------------------
# Transform dataframe to an numpy array:
#
# Input variables:
# df:       type(dataframe): Dataframe
#
# Return variables:
# df_numpy: type(list):      Numpy array of the dataframe
#

def dataframe_numpy(df):

   df_numpy = df.to_numpy()
   return df_numpy



#--------------------------------------------------------------------------------------------------
# Fast statistic of the dataframe
#
# Input variables:
# df:                type(dataframe): Dataframe
#
# Return variables:
# df_fast_statistic: type(dataframe): Dataframe of a rough statistic overview of the dataset
#

def dataframe_fast_statistic(df):

   df_fast_statistic = df.describe()
   return df_fast_statistic



#--------------------------------------------------------------------------------------------------
# Transposing the dataframe 
#
# Input variables:
# df:           type(dataframe): Dataframe
#
# Return variables:
# df_transpose: type(dataframe): Transposed dataframe
#

def dataframe_transpose(df):

   df_transpose = df.T
   return df_transpose



#--------------------------------------------------------------------------------------------------
# Sorting dataframe by column index or column name
#
# Input variables:
# df:          type(dataframe): Dataframe
# sort_option: type(str/int):   Option for sorting the dataframe entries (string for column name or integer for column indices (0=index, 1=column)
#
# Return variables:
# df_sorted:   type(dataframe): Sorted dataframe
#

def sort_dataframe(df, sort_option, **kwargs):

   if (type(sort_option) == int):   # Sorting the dataset by the given column integer index
      if "sort_kwargs" in kwargs:
         df_sorted = df.sort_index(axis=sort_option,**kwargs["sort_kwargs"])
      else:
         df_sorted = df.sort_index(axis=sort_option)
   elif (type(sort_option) == str):   # Sorting the dataset by the given row string index
      if "sort_kwargs" in kwargs:
         df_sorted = df.sort_values(by=str(sort_option),**kwargs["sort_kwargs"])
      else:
         df_sorted = df.sort_values(by=str(sort_option))
   else:
      outp.message_line_array(["Wrong type in position 2!","It has to be type str or int!"],headword="Fatal error")
   return df_sorted



#--------------------------------------------------------------------------------------------------
# Get certain colomn of a panda dataframe chosen by first column name as index:
#
# Input variables:
# df:             type(dataframe): Dataframe
# rows_select:    type(list):      Input list to select the rows - rows_select=[string,type]
# columns_select: type(list):      Input list to select the columns - columns_select=[string,type] 
#
# Return variables:
# df.slice:       type(dataframe): Sliced dataframe
#

def dataframe_edit(df, rows_select, columns_select, get_set, *args):

   #---------------------------------------------------------------------------
   # Subfunction - label string with empty row argument:
   def dataframe_edit_via_label_string_empty_rows(df, rows_select, columns_select, get_set, *args):
      df_mod = df.copy()
      rows_colon_split = rows_select[0].split(":")
      rows_comma_split = rows_select[0].split(",")
      columns_colon_split = columns_select[0].split(":")
      columns_comma_split = columns_select[0].split(",")
      if len(rows_colon_split) > 1:
         if isinstance(df.index[0],datetime.datetime):
            rows_colon_split = [pd.to_datetime(ii) for ii in rows_colon_split]
      if len(columns_colon_split) > 1:
         if isinstance(df.columns[0],datetime.datetime):
            columns_colon_split = [pd.to_datetime(ii) for ii in columns_colon_split]
      if (len(rows_comma_split) != 0 and len(rows_colon_split) < 2):
         if isinstance(df.index[0],datetime.datetime):
            rows_comma_split = [pd.to_datetime(ii) for ii in rows_comma_split]
      if (len(columns_comma_split) != 0 and len(columns_colon_split) < 2):
         if isinstance(df.columns[0],datetime.datetime):
            columns_comma_split = [pd.to_datetime(ii) for ii in column_comma_split]
      if (len(columns_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.loc[:,columns_colon_split[0]:columns_colon_split[1]]
            if __name__ == "__main__":
               print('case 1')
         elif (get_set == "set"):
            items_number_check(args, df.loc[:,columns_colon_split[0]:columns_colon_split[1]])
            df_mod.loc[:,columns_colon_split[0]:columns_colon_split[1]] = args[0]
            if __name__ == "__main__":
               print('case 2')
      elif (len(columns_comma_split) == 1):
         if (get_set == "get"):
            df_mod = df.loc[:,columns_comma_split[0]]
            if __name__ == "__main__":
               print('case 3')
         elif (get_set == "set"):
            items_number_check(args, df.loc[:,columns_comma_split[0]])
            df_mod.loc[:,columns_comma_split] = args[0]
            if __name__ == "__main__":
               print('case 4')
      else:
         if (get_set == "get"):
            df_mod = df.loc[:,columns_comma_split]
            if __name__ == "__main__":
               print('case 5')
         elif (get_set == "set"):
            items_number_check(args, df.loc[:,columns_comma_split])
            df_mod.loc[:,columns_comma_split] = args[0]
            if __name__ == "__main__":
               print('case 6')
      return df_mod

   #---------------------------------------------------------------------------
   # Subfunction - label string with empty column argument:
   def dataframe_edit_via_label_string_empty_columns(df, rows_select, columns_select, get_set, *args):
      df_mod = df.copy()
      rows_colon_split = rows_select[0].split(":")
      rows_comma_split = rows_select[0].split(",")
      columns_colon_split = columns_select[0].split(":")
      columns_comma_split = columns_select[0].split(",")
      if len(rows_colon_split) > 1:
         if isinstance(df.index[0],datetime.datetime):
            rows_colon_split = [pd.to_datetime(ii) for ii in rows_colon_split]
      if len(columns_colon_split) > 1:
         if isinstance(df.columns[0],datetime.datetime):
            columns_colon_split = [pd.to_datetime(ii) for ii in columns_colon_split]
      if (len(rows_comma_split) != 0 and len(rows_colon_split) < 2):
         if isinstance(df.index[0],datetime.datetime):
            rows_comma_split = [pd.to_datetime(ii) for ii in rows_comma_split]
      if (len(columns_comma_split) != 0 and len(columns_colon_split) < 2):
         if isinstance(df.columns[0],datetime.datetime):
            columns_comma_split = [pd.to_datetime(ii) for ii in colums_comma_split]
      if (len(rows_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.loc[rows_colon_split[0]:rows_colon_split[1],:]
            if __name__ == "__main__":
               print('case 7')
         elif (get_set == "set"):
            items_number_check(args, df.loc[rows_colon_split[0]:rows_colon_split[1]])
            count = 0
            for ii in dataframe_index(df.loc[rows_colon_split[0]:rows_colon_split[1]]):
               df_mod.loc[ii,:] = args[0][count]
               count += 1
            if __name__ == "__main__":
               print('case 8')
      elif (len(rows_comma_split) == 1):
         if (get_set == "get"):
            df_mod = df.loc[rows_comma_split[0]]
            if __name__ == "__main__":
               print('case 9')
         elif (get_set == "set"):
            items_number_check(args, df.loc[rows_comma_split[0],:])
            args_ = []
            for ii in args[0]:
               args_.append(ii[0])
            df_mod.loc[rows_comma_split[0],:] = args_
            if __name__ == "__main__":
               print('case 10')
      else:
         if (get_set == "get"):
            df_mod = df.loc[rows_comma_split,:]
            if __name__ == "__main__":
               print('case 11')
         elif (get_set == "set"):
            items_number_check(args,df.loc[rows_comma_split,:])
            count = 0
            for ii in dataframe_index(df.loc[rows_comma_split,:]):
               df_mod.loc[ii,:] = args[0][count]
               count += 1
            if __name__ == "__main__":
               print('case 12')
      return df_mod

   #---------------------------------------------------------------------------
   # Subfunction:
   def dataframe_edit_via_label_string_rows_and_columns(df, rows_select, columns_select, get_set, *args):
      df_mod = df.copy()
      rows_colon_split = rows_select[0].split(":")
      rows_comma_split = rows_select[0].split(",")
      columns_colon_split = columns_select[0].split(":")
      columns_comma_split = columns_select[0].split(",")
      if len(rows_colon_split) > 1:
         if isinstance(df.index[0],datetime.datetime):
            rows_colon_split = [pd.to_datetime(ii) for ii in rows_colon_split]
      if len(columns_colon_split) > 1:
         if isinstance(df.columns[0],datetime.datetime):
            columns_colon_split = [pd.to_datetime(ii) for ii in columns_colon_split]
      if (len(rows_comma_split) != 0 and len(rows_colon_split) < 2):
         if isinstance(df.index[0],datetime.datetime):
            rows_comma_split = [pd.to_datetime(ii) for ii in rows_comma_split]
      if (len(columns_comma_split) != 0 and len(columns_colon_split) < 2):
         if isinstance(df.columns[0],datetime.datetime):
            columns_comma_split = [pd.to_datetime(ii) for ii in colums_comma_split]
      if (len(rows_colon_split) > 1 and len(columns_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.loc[rows_colon_split[0]:rows_colon_split[1],columns_colon_split[0]:columns_colon_split[1]]
            if __name__ == "__main__":
               print('case 13')
         elif (get_set == "set"):
            items_number_check(args,df.loc[rows_colon_split[0]:rows_colon_split[1],columns_colon_split[0]:columns_colon_split[1]])
            df_mod.loc[rows_colon_split[0]:rows_colon_split[1],columns_colon_split[0]:columns_colon_split[1]] = args[0]
            if __name__ == "__main__":
               print('case 14')
      elif (len(rows_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.loc[rows_colon_split[0]:rows_colon_split[1],columns_comma_split]
            if __name__ == "__main__":
               print('case 15')
         elif (get_set == "set"):
            items_number_check(args,df.loc[rows_colon_split[0]:rows_colon_split[1],columns_colon_split])
            df_mod.loc[rows_colon_split[0]:rows_colon_split[1],columns_colon_split] = args[0]
            if __name__ == "__main__":
               print('case 16')
      elif (len(columns_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.loc[rows_comma_split,columns_colon_split[0]:columns_colon_split[1]]
            if __name__ == "__main__":
               print('case 17')
         if (get_set == "set"):
            items_number_check(args,df.loc[rows_comma_split,columns_colon_split[0]:columns_colon_split[1]])
            columns_colon_split = dataframe_columns(df.loc[rows_comma_split,columns_colon_split[0]:columns_colon_split[1]])
            count = 0
            for ii in columns_colon_split:
               df_mod.loc[rows_comma_split,ii] = args[0][count]
               count += 1
            if __name__ == "__main__":
               print('case 18')
      elif (len(rows_comma_split) == 1):
         if (get_set == "get"):
            df_mod = df.loc[rows_comma_split[0],columns_comma_split]
            if __name__ == "__main__":
               print('case 19')
         elif (get_set == "set"):
            items_number_check(args,df.loc[rows_comma_split[0],columns_comma_split[0]])
            df_mod.loc[rows_comma_split[0],columns_comma_split[0]] = args[0][0]
            if __name__ == "__main__":
               print('case 20')
      else:
         if (get_set == "get"):
            df_mod = df.loc[rows_comma_split,columns_comma_split]
            if __name__ == "__main__":
               print('case 21')
         elif (get_set == "set"):
            items_number_check(args,df.loc[rows_comma_split,columns_comma_split])
            df_mod.loc[rows_comma_split,columns_comma_split] = args[0]
            if __name__ == "__main__":
               print('case 22')
      return df_mod 

   #---------------------------------------------------------------------------
   # Subfunction:
   def dataframe_edit_via_index_integer_empty_rows(df, rows_select, columns_select, get_set, *args):
      df_mod = df.copy()
      rows_colon_split = rows_select[0].split(":")
      rows_comma_split = rows_select[0].split(",")
      columns_colon_split = columns_select[0].split(":")
      columns_comma_split = columns_select[0].split(",")
      if (len(columns_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.iloc[:,int(columns_colon_split[0]):int(columns_colon_split[1])]
            if __name__ == "__main__":
               print('case 23')
         elif (get_set == "set"):
            items_number_check(args, df.iloc[:,int(columns_colon_split[0]):int(columns_colon_split[1])])
            df_mod.iloc[:,int(columns_colon_split[0]):int(columns_colon_split[1])] = args[0]
            if __name__ == "__main__":
               print('case 24')
      elif (len(columns_comma_split) == 1):
         if (get_set == "get"):
            df_mod = df.iloc[:,int(columns_comma_split[0])]
            if __name__ == "__main__":
               print('case 25')
         elif (get_set == "set"):
            items_number_check(args,df_mod.iloc[:,int(columns_comma_split[0])])
            args_ = []
            for ii in args[0]:
               args_.append(ii[0])
            df_mod.iloc[:,int(columns_comma_split[0])] = args_
            if __name__ == "__main__":
               print('case 26')
      else:
         columns_comma_split = [int(ii) for ii in columns_comma_split]
         if (get_set == "get"):
            columns_comma_split = [int(ii) for ii in columns_comma_split]
            df_mod = df.iloc[:,columns_comma_split]
            if __name__ == "__main__":
               print('case 27')
         elif (get_set == "set"):
            items_number_check(args,df_mod.iloc[:,columns_comma_split])
            df_mod.iloc[:,columns_comma_split] = args[0]
            if __name__ == "__main__":
               print('case 28')
      return df_mod

   #---------------------------------------------------------------------------
   # Subfunction:
   def dataframe_edit_via_index_integer_empty_columns(df, rows_select, columns_select, get_set, *args):
      df_mod = df.copy()
      rows_colon_split = rows_select[0].split(":")
      rows_comma_split = rows_select[0].split(",")
      columns_colon_split = columns_select[0].split(":")
      columns_comma_split = columns_select[0].split(",")
      if (len(rows_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),:]
            if __name__ == "__main__":
               print('case 29')
         elif (get_set == "set"):
            items_number_check(args,df_mod.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),:])
            df_mod.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),:] = args[0]
            if __name__ == "__main__":
               print('case 30')
      elif (len(rows_comma_split) == 1):
         if (get_set == "get"):
            df_mod = df.iloc[int(rows_comma_split[0])]
            if __name__ == "__main__":
               print('case 31')
         elif (get_set == "set"):
            items_number_check(args,df_mod.iloc[int(rows_comma_split[0]),:])
            args_ = []
            for ii in args[0]:
               args_.append(ii[0])
            df_mod.iloc[int(rows_comma_split[0]),:] = args_
            if __name__ == "__main__":
               print('case 32')
      else:
         rows_comma_split = [int(ii) for ii in rows_comma_split]
         if (get_set == "get"):
            df_mod = df.iloc[rows_comma_split,:]
            if __name__ == "__main__":
               print('case 33')
         elif (get_set == "set"):
            items_number_check(args,df.iloc[rows_comma_split,:])
            df_mod.iloc[rows_comma_split,:] = args[0]
            if __name__ == "__main__":
               print('case 34')
      return df_mod

   #---------------------------------------------------------------------------
   # Subfunction:
   def dataframe_edit_via_index_integer_rows_and_columns(df, rows_select, columns_select, get_set, *args):
      df_mod = df.copy()
      rows_colon_split = rows_select[0].split(":")
      rows_comma_split = rows_select[0].split(",")
      columns_colon_split = columns_select[0].split(":")
      columns_comma_split = columns_select[0].split(",")
      if (len(rows_colon_split) > 1 and len(columns_colon_split) > 1):
         if (get_set == "get"):
            df_mod = df.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),int(columns_colon_split[0]):int(columns_colon_split[1])]
            if __name__ == "__main__":
               print('case 35')
         elif (get_set == "set"):
            items_number_check(args,df.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),int(columns_colon_split[0]):int(columns_colon_split[1])])
            count_ii = 0
            for ii in range(int(rows_colon_split[0]),int(rows_colon_split[1])):
               count_jj = 0
               for jj in range(int(columns_colon_split[0]),int(columns_colon_split[1])):
                  df_mod.iloc[ii,jj] = args[0][count_ii][count_jj]
                  count_jj += 1
               count_ii += 1
            if __name__ == "__main__":
               print('case 36')
      elif (len(rows_colon_split) > 1): 
         columns_comma_split = [int(ii) for ii in columns_comma_split]
         if (get_set == "get"):
            df_mod = df.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),columns_comma_split]
            if __name__ == "__main__":
               print('case 37')
         elif (get_set == "set"):
            items_number_check(args,df_mod.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),columns_comma_split])
            df_mod.iloc[int(rows_colon_split[0]):int(rows_colon_split[1]),columns_comma_split] = args[0]
            if __name__ == "__main__":
               print('case 38')
      elif (len(columns_colon_split) > 1):
         rows_comma_split = [int(ii) for ii in rows_comma_split]
         if (get_set == "get"):
            df_mod = df.iloc[rows_comma_split,int(columns_colon_split[0]):int(columns_colon_split[1])]
            if __name__ == "__main__":
               print('case 39')
         elif (get_set == "set"):
            items_number_check(args,df.iloc[rows_comma_split,int(columns_colon_split[0]):int(columns_colon_split[1])])
            df_mod.iloc[rows_comma_split,int(columns_colon_split[0]):int(columns_colon_split[1])] = args[0]
            if __name__ == "__main__":
               print('case 40')
      else:
         rows_comma_split = [int(ii) for ii in rows_comma_split]
         columns_comma_split = [int(ii) for ii in columns_comma_split]
         if (get_set == "get"):
            df_mod = df.iloc[rows_comma_split,columns_comma_split]
            if __name__ == "__main__":
               print('case 41')
         elif (get_set == "set"):
            items_number_check(args,df.iloc[rows_comma_split,columns_comma_split])
            df_mod.iloc[rows_comma_split,columns_comma_split] = args[0]
            if __name__ == "__main__":
               print('case 42')
      return df_mod


   #---------------------------------------------------------------------------
   # Subfunction:
   def dataframe_edit_via_label_string(df, rows_select, columns_select, get_set, *args):
      if (len(rows_select[0]) == 0 and len(columns_select[0]) > 0):
         df_mod = dataframe_edit_via_label_string_empty_rows(df, rows_select, columns_select, get_set, *args)
      elif (len(rows_select[0]) > 0 and len(columns_select[0]) == 0):
         df_mod = dataframe_edit_via_label_string_empty_columns(df, rows_select, columns_select, get_set, *args)
      elif (len(rows_select[0]) > 0 and len(columns_select[0]) > 0):
         df_mod = dataframe_edit_via_label_string_rows_and_columns(df, rows_select, columns_select, get_set, *args)
      else:
         outp.message_line("Rows and columns selection must not be both empty!",headword="Fatal error")
      return df_mod

   #---------------------------------------------------------------------------
   # Subfunction:
   def dataframe_edit_via_integer_index(df, rows_select, columns_select, get_set, *args):
      if (len(rows_select[0]) == 0 and len(columns_select[0]) > 0):
         df_mod = dataframe_edit_via_index_integer_empty_rows(df, rows_select, columns_select, get_set, *args)
      elif (len(rows_select[0]) > 0 and len(columns_select[0]) == 0):
         df_mod = dataframe_edit_via_index_integer_empty_columns(df, rows_select, columns_select, get_set, *args)
      elif (len(rows_select[0]) > 0 and len(columns_select[0]) > 0):
         df_mod = dataframe_edit_via_index_integer_rows_and_columns(df, rows_select, columns_select, get_set, *args)
      else:
         outp.message_line("Rows and columns selection must not be both empty!",headword="Fatal error")
      return df_mod

   #---------------------------------------------------------------------------
   # Subfunction:
   def items_number_check(args, dataframe):
      if (gop.all_items_number(args[0]) != dataframe_size(dataframe)):
         outp.message_line_array(["Number of dataframe numbers that should be edited and", \
                                  "new variable list have to be equal!"],headword="Fatal error")


   #---------------------------------------------------------------------------
   # Main function:
   if ((get_set != "get" and get_set != "set") or type(get_set) != str):
      outp.message_line_array(["Wrong statement in argument position 4!","It has to be optional \"get\" or \"set\"!"],headword="Fatal error")
   if (get_set == "set" and not args):
      outp.message_line("New dataframe variable list has to be set in argument position 5!",headword="Fatal error")
   if (type(rows_select[0]) != str and type(columns_select[0] != str)):
      outp.message_line("Row and column selection arguments have to be strings!",headword="Fatal error")
   elif (type(rows_select[1]) != type(columns_select[1])):
      outp.message_line("Row and column type arguments have to be the same!",headword="Fatal error")
   else:
      if (rows_select[1] == str and columns_select[1] == str):
         df_mod = dataframe_edit_via_label_string(df, rows_select, columns_select, get_set, *args)
      elif (rows_select[1] == int and columns_select[1] == int):
         df_mod = dataframe_edit_via_integer_index(df, rows_select, columns_select, get_set, *args)
      return df_mod



#--------------------------------------------------------------------------------------------------
# Filter dataframe 
#
# Input variables:
# df:          type(dataframe): Dataframe
# column_name: type(str):       string name of a dataframe column
# filter_list: type(list):      Filter list with relational operators or index labels
#
# Return variables:
# df_filtered: type(dataframe): Sorted dataframe
#

def dataframe_filter(df, column_name, filter_list):

   if (filter_list[0] == "=="):
      if (column_name == ""):
         df_filtered = df[df == filter_list[1]]
      else:
         df_filtered = df[df[column_name] == filter_list[1]]
   elif (filter_list[0] == ">"):
      if (column_name == ""):
         df_filtered = df[df > filter_list[1]]
      else:
         df_filtered = df[df[column_name] > filter_list[1]]
   elif (filter_list[0] == "<"):
      if (column_name == ""):
         df_filtered = df[df < filter_list[1]]
      else:
         df_filtered = df[df[column_name] < filter_list[1]]
   else:
      outp.message_line("Filter option has to be a list of strings!",headword="Fatal error")
   return df_filtered



#--------------------------------------------------------------------------------------------------
# Set dataframe element
#
# Input variables:
# df:      type(dataframe): Dataframe
# indices: type(str/int):   Cut direction (top/bottom)
# element: type(...):       Number of cutted rows
#
# Return variables:
# df_mod:  type(dataframe): Updated dataframe
#

def dataframe_set(df, indices, element):
   
   df_mod = df.copy()
   if (type(indices[0]) == int and type(indices[1]) == int):
      df_mod.iat[indices[0],indices[1]] = element
      return df_mod
   elif (type(indices[0]) == str and type(indices[1]) == str):
      df_mod.at[indices[0],indices[1]] = element
      return df_mod
   else:
      outp.message_line("Indices list has to be two integers or strings!",headword="Fatal error")






###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################

if __name__ == "__main__":

   import sys

   outp.message_empty_line()
   outp.message_box_array(["","Python and Pandas version",""], 100, align="center")
   outp.message_empty_line()
   outp.message_line("Python version:")
   outp.output_object(sys.version)
   outp.message_empty_line()
   outp.message_line("Version info:")
   outp.output_object(sys.version_info)
   outp.message_empty_line()
   outp.message_line("System path:")
   outp.output_object(sys.path)
   outp.message_empty_line()
   outp.message_line("Pandas version:")
   outp.output_object(pd.__version__)
   outp.message_empty_line()
   outp.message_empty_line()
   outp.message_empty_line()
   outp.message_box_array(["", "Testing dataset operation functions",""], 100, align="center")
   outp.message_empty_line()
   outp.message_empty_line()  

   #dates = pd.date_range('20130101', periods=6)
   #df1 = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list('ABCD'))
   #outp.message_empty_line()
   #print(df1)
   #df1.to_csv('test_df1.csv')

   df1 = read_csv_dataframe('test_df1.csv', df_kwargs={"dtype":{"A":"float64","B":"float64","C":"float64","D":"float64"}, \
                            "index_col":0, "parse_dates":[0], "date_parser":pd.to_datetime}, set_freq="True")

   #df2 = pd.DataFrame({'A': 1., \
   #                    'B': pd.Timestamp('20130102'), \
   #                    'C': pd.Series(1, index=list(range(4)), dtype='float32'), \
   #                    'D': np.array([3] * 4, dtype='int32'), \
   #                    'E': pd.Categorical(["test", "train", "test", "train"]), \
   #                    'F': 'foo'})
   #print(df2)
   #df2.to_csv('test_df2.csv')

   df2 = read_csv_dataframe('test_df2.csv', df_kwargs={"dtype":{"A":"float64","B":"str","C":"float32","D":"int32","D":"category","E":"object"}, \
                            "index_col":0, "parse_dates":["B"], "date_parser":pd.to_datetime})

   outp.message_empty_line()
   outp.message_box_array(["Testing function: read_csv_dataframe('test_df1.csv', df_kwargs={...}, set_freq=\"True\""],100)
   outp.message_empty_line()
   outp.output_object(df1)
   outp.message_empty_line()
   outp.output_object(df2)
   outp.message_empty_line()
   outp.output_object(df1.dtypes)
   outp.message_empty_line()
   outp.output_object(df2.dtypes)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["cut_dataframe(df, cut_dir, cut_nr)"],100)
   outp.message_empty_line()
   outp.output_object(cut_dataframe(df1,"top",3),comment_line="Out:")
   outp.message_empty_line()
   outp.output_object(cut_dataframe(df2,"bottom",2))
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["dataframe_columns(df)"],100)
   outp.message_empty_line()
   outp.output_object(dataframe_columns(df1))
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["dataframe_index(df)"],100)
   outp.message_empty_line()
   outp.output_object(dataframe_index(df1))
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["dataframe_numpy(df)"],100)
   outp.message_empty_line()
   df1_numpy = dataframe_numpy(df1)
   outp.output_object(df1_numpy)
   outp.message_empty_line()
   df2_numpy = dataframe_numpy(df2)
   outp.output_object(df2_numpy)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["dataframe_fast_statistic(df)"],100)
   outp.message_empty_line()
   df1_fs = dataframe_fast_statistic(df1)
   outp.output_object(df1_fs)
   outp.message_empty_line()
   df2_fs = dataframe_fast_statistic(df2)
   outp.output_object(df2_fs)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["dataframe_transpose(df)"],100)
   outp.message_empty_line()
   df1_t = dataframe_transpose(df1)
   outp.output_object(df1_t)
   outp.message_empty_line()
   df2_t = dataframe_transpose(df2)
   outp.output_object(df2_t)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["sort_dataframe(df, sort_option, **kwargs)"],100)
   outp.message_empty_line()
   df1_sorted = sort_dataframe(df1,0,sort_kwargs={"ascending":False})
   outp.output_object(df1_sorted)
   outp.message_empty_line()
   df1_sorted = sort_dataframe(df1,1,sort_kwargs={"ascending":False})
   outp.output_object(df1_sorted)
   outp.message_empty_line()
   df1_sorted = sort_dataframe(df1,'B')
   outp.output_object(df1_sorted)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["def dataframe_edit(df, rows_select, columns_select, get_set, *args)"],100)
   outp.message_empty_line()


   count=0
   #---------------------------------------------------------------------------

   # String version: Get column by label string
   count += 1
   outp.message_line("Get column by label string (String version):")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"\",str],[\"A\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["",str],["A",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version: Get column by index integer
   count += 1
   outp.message_line("Get column by index integer (Integer version):")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"\",int],[\"0\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["",int],["0",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version: Get column by label string range
   count += 1
   outp.message_line("Get column by label string range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"\",str],[\"A:C\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["",str],["A:C",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # String version: Get column by index integer range
   count += 1
   outp.message_line("Get column by index integer range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"\",int],[\"0:3\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["",int],["0:3",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version: Get column by label string list
   count += 1
   outp.message_line("Get column by label string list:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"\",str],[\"A,B,C\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["",str],["A,B,C",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version: Get column by index integer list:
   count += 1
   outp.message_line("Get column by index integer list:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"\",int],[\"0,1,2\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["",int],["0,1,2",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version: Get row by label string
   count += 1
   outp.message_line("Get row by label string:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130104\",str],[\"\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130104",str],["",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version: Get row by index integer
   count += 1
   outp.message_line("Get row by index integer:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"3\",int],[\"\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["3",int],["",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version: Get row by label string range
   count += 1
   outp.message_line("Get row by label string range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102:20130104\",str],[\"\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102:20130104",str],["",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version: Get row by index integer range
   count += 1
   outp.message_line("Get row by index integer range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1:4\",int],[\"\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["1:4",int],["",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version: Get column by label string range
   count += 1
   outp.message_line("Get row by label string list:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102,20130103,20130104\",str],[\"\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102,20130103,20130104",str],["",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integeger version: Get row by index integer list
   count += 1
   outp.message_line("Get row by index integer list:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1,2,3\",int],[\"\",int]),\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["1,2,3",int],["",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String Version: Get element by row and column label string
   count += 1
   outp.message_line("Get element by row and column label string:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130104\",str],[\"A\",str])",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130104",str],["A",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer Version: Get element by row and column index integer
   count += 1
   outp.message_line("Get element by row and column index integer:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"3\",int],[\"0\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["3",int],["0",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String Version: Get slice by row label string range and by column label string
   count += 1
   outp.message_line("Get slice by row label string range and by column label string:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102:20130104\",str],[\"A\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102:20130104",str],["A",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer Version: Get slice by row index integer range and by column index integer
   count += 1
   outp.message_line("Get slice by row index integer range and by column index integer:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1:4\",int],[\"0\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["1:4",int],["0",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String Version: Get slice by row and column label string range
   count += 1
   outp.message_line("Get slice by row label string range and by column label string:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102\",str],[\"A:C\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102",str],["A:C",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer Version: Get slice by row and column index integer range
   count += 1
   outp.message_line("Get slice by row index integer range and by column index integer:")
   outp.message_empty_line()	
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1\",int],[\"0:3\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["1",int],["0:3",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String Version: Get slice by row and column label string range
   count += 1
   outp.message_line("Get slice by row and column label string range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102:20130104\",str],[\"A:C\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102:20130104",str],["A:C",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer Version: Get slice by row and column index integer range
   count += 1
   outp.message_line("Get slice by row and column index integer range:")
   outp.message_empty_line()	
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1:4\",int],[\"0:3\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["1:4",int],["0:3",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String Version: Get slice by row label list and by column label string
   count += 1
   outp.message_line("Get slice by row label string list and by column label string:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130101,20130103,20130105\",int],[\"B\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130101,20130103,20130105",str],["B",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer Version: Get slice by row index integer list and by column index integer
   count += 1
   outp.message_line("Get slice by row index integer list and by column index integer:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"0,1,2\",int],[\"1\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["0,1,2",int],["1",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String Version: Get slice by row and column label string list
   count += 1
   outp.message_line("Get slice by row and column label string list:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130101,20130103,20130104\",str],[\"B,D\",str],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130101,20130103,20130104",str],["B,D",str],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer Version: Get slice by row and column index integer list
   count += 1
   outp.message_line("Get slice by row and column index integer list:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"0,2,3\",int],[\"1,3\",int],\"get\")",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["0,2,3",int],["1,3",int],"get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------
   # String version: Set column by label string
   count += 1
   outp.message_line("Set column by label string:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"\",str],[\"B\",str],\"set\", \\
          [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)],[np.float64(5.0)],[np.float64(6.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)],[np.float64(5.0)],[np.float64(6.0)]]
   df1_mod = dataframe_edit(df1,["",str],["B",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version: Set column by index integer
   count += 1
   outp.message_line("Set column by index integer:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"\",int],[\"1\",int],\"set\", \\
          [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)],[np.float64(5.0)],[np.float64(6.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)],[np.float64(5.0)],[np.float64(6.0)]]
   df1_mod = dataframe_edit(df1,["",int],["1",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version: Set column by label string range
   count += 1
   outp.message_line("Set column by label string range:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"\",str],[\"A:C\",str],\"set\" \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \\
           [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \
            [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]]
   df1_mod = dataframe_edit(df1,["",str],["A:C",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version: Get column by index integer range
   count += 1
   outp.message_line("Get column by index integer range:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"\",int],[\"0:3\",int],\"set\"\\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \\
           [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \
            [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]]
   df1_mod = dataframe_edit(df1,["",int],["0:3",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   outp.message_line("Set column by label string list:")
   count += 1
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"\",str],[\"A,B,C\",str],,\"set\" \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \\
           [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \
            [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]]
   df1_mod = dataframe_edit(df1,["",str],["A,B,C",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   outp.message_line("Set column by index list:")
   count += 1
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"\",int],[\"0,1,2\",int],,\"set\" \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \\
           [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)],[np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)],[np.float64(10.0),np.float64(11.0),np.float64(12.0)], \
            [np.float64(13.0),np.float64(14.0),np.float64(15.0)],[np.float64(16.0),np.float64(17.0),np.float64(18.0)]]
   df1_mod = dataframe_edit(df1,["",int],["0,1,2",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   outp.message_line("Set row by label string:")
   count += 1
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"20130104\",str],[\"\",str],\"set\" \\
          [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)]]
   df1_mod = dataframe_edit(df1,["20130104",str],["",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   outp.message_line("Set row by index string:")
   count += 1
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"1\",int],[\"\",int],\"set\" \\
          [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)],[np.float64(4.0)]]
   df1_mod = dataframe_edit(df1,["3",int],["",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Set row by label string range:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"20130102:20130104\",str],[\"\",str],\"set\", \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \\
           [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \\
           [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \
            [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \
            [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]]
   df1_mod = dataframe_edit(df1,["20130102:20130104",str],["",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Set row by index integer range:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"1:4\",int],[\"\",int],\"set\" \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \\
           [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \\
           [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \
            [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \
            [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]]
   df1_mod = dataframe_edit(df1,["1:4",int],["",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Set row by label string list:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"20130102,20130103,20130104\",int],[\"\",int], \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \\
           [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \\
           [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]],\"set\")""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \
            [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \
            [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]]
   df1_mod = dataframe_edit(df1,["20130102,20130103,20130104",str],["",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Set row by index integer list:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"1,2,3\",int],[\"\",int], \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \\
           [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \\
           [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]],\"set\")""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0),np.float64(4.0)], \
            [np.float64(5.0),np.float64(6.0),np.float64(7.0),np.float64(8.0)], \
            [np.float64(9.0),np.float64(10.0),np.float64(11.0),np.float64(12.0)]]
   df1_mod = dataframe_edit(df1,["1,2,3",int],["",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Get element by row and column label:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130104\",str],[\"A\",str],\"set\",[1.0])",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130104",str],["A",str],"set",[1.0])
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Get element by row and column index:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"3\",int],[\"0\",int],\"set\",[1.0])",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["3",int],["0",int],"set",[1.0])
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Get slice by row label range and by column label:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102:20130104\",str],[\"A\",str],\"set\",[[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]])",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102:20130104",str],["A",str],"set",[[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]])
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Get slice by row index range and by column index:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1:4\",int],[\"0\",int],\"set\",[[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]]",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["1:4",int],["0",int],"set",[[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]])
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Get slice by row index list and by column index:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"20130102,20130104,20130105\",str],[\"A\",str],\"set\", \\
          [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]]
   df1_mod = dataframe_edit(df1,["20130102,20130104,20130105",str],["A",str],"set", array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Get slice by row index list and by column index:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"1,3,4\",int],[\"1\",int],\"set\", \\
          [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]])""",comment_line="In:")
   outp.message_empty_line()
   array = [[np.float64(1.0)],[np.float64(2.0)],[np.float64(3.0)]]
   df1_mod = dataframe_edit(df1,["1,3,4",int],["0",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Get slice by row and column label range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"20130102\",str],[\"A:C\",str],\"set\",[np.float64(1.0),np.float64(2.0),np.float64(3.0)])",comment_line="In:")
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102",str],["A:C",str],"set",[np.float64(1.0),np.float64(2.0),np.float64(3.0)])
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Get slice by row and column index range:")
   outp.message_empty_line()
   outp.message_line("df1_mod = dataframe_edit(df1,[\"1\",int],[\"0:3\",int],\"set\",[np.float64(1.0),np.float64(2.0),np.float64(3.0)])",comment_line="In:")
   df1_mod = dataframe_edit(df1,["1",int],["0:3",int],"set",[np.float64(1.0),np.float64(2.0),np.float64(3.0)])
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Get slice by row and column label range:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"20130102:20130104\",str],[\"A:C\",str],\"set\", \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \\
           [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)]])""",comment_line="In:")
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \
            [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)]]
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102:20130104",str],["A:C",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Get slice by row and column index range:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"1:4\",int],[\"0:3\",int],\"set\", \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \\
           [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)]])""",comment_line="In:")
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \
            [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)]]
   df1_mod = dataframe_edit(df1,["1:4",int],["0:3",int],"set",array)
   outp.message_empty_line()
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_character_line("*",70)
   outp.message_empty_line()

   #---------------------------------------------------------------------------

   # String version
   count += 1
   outp.message_line("Get slice by row and column label list:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"20130102,20130103,20130104\",str],[\"A,B,C\",str],\"set\", \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \\
           [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)]])""",comment_line="In:")
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \
            [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)]]
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["20130102,20130103,20130104",str],["A,B,C",str],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   # Integer version
   count += 1
   outp.message_line("Get slice by row and column integer list:")
   outp.message_empty_line()
   outp.message_line("""df1_mod = dataframe_edit(df1,[\"1,2,3\",int],[\"0,1,2\",int],\"set\", \\
          [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \\
           [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \\
           [np.float64(7.0),np.float64(8.0),np.float64(9.0)]])""",comment_line="In:")
   array = [[np.float64(1.0),np.float64(2.0),np.float64(3.0)], \
            [np.float64(4.0),np.float64(5.0),np.float64(6.0)], \
            [np.float64(7.0),np.float64(8.0),np.float64(9.0)]]
   outp.message_empty_line()
   df1_mod = dataframe_edit(df1,["2,3,4",int],["0,1,2",int],"set",array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()
   print('count: ', count)
   exit()


   df1_filter = dataframe_filter(df1, "A", [">", 0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   df1_filter = dataframe_filter(df1, "A", ["<", 0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   df1_filter = dataframe_filter(df1, "", [">", 0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   df1_filter = dataframe_filter(df1, "", ["<", 0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   df1_new = dataframe_set(df1, ["20130104","A"], 1.0)
   outp.output_object(df1,comment_line="Out:")
   outp.message_empty_line()
   df1_new = dataframe_set(df1, [2,1], 1.0)
   outp.output_object(df1,comment_line="Out:")


