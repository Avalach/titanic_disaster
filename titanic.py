import numpy  as np
import pandas as pd

from python_ml_files               import read_csv_file        as rcf
from python_ml_files               import array_op             as ao
from python_ml_files.read_csv_file import csv_dataframe        as csv_df
from python_ml_files               import output               as outp
from collections                   import defaultdict, Counter

csv_file_first_row = rcf.read_csv_file_first_row("train.csv", ",")

column_types = ['integer', 'boolean', 'integer', 'string', 'string', 'string', 'integer', 'integer', 'string', 'float', 'string', 'string']

csv_columns = rcf.read_csv_file_array("train.csv", ",", "", "INVALID", column_types)

#print(csv_columns_str_max)

x_array_0_1, y_array_0_1 = ao.x_y_array(csv_columns, csv_file_first_row[0], csv_file_first_row[5])

Survived_counter = ao.counter_array(csv_columns, csv_file_first_row[1])
#print(Survived_counter[False], Survived_counter[True])

Pclass_counter = ao.counter_array(csv_columns, csv_file_first_row[2])
#print(Pclass_counter[1], Pclass_counter[2], Pclass_counter[3])

Sex_counter = ao.counter_array(csv_columns, csv_file_first_row[4])
#print(Sex_counter["male"], Sex_counter["female"])

Embarked_counter = ao.counter_array(csv_columns, csv_file_first_row[11])
#print(Embarked_counter["S"], Embarked_counter["C"], Embarked_counter["Q"])

#df = pd.DataFrame()
#df = rcf.read_csv_file_make_dataframe("train.csv")

#df_column  = rcf.dataframe_column(df, "Pclass")

#print(df.iloc[:,0])

#first_row = rcf.dataframe_first_row(df)
#print(csv_file_first_row)
#print(first_row)

csv_df = csv_df("train.csv",",","","INVALID")

#csv_column, csv_column_max_str = csv_df.data_array
#print(csv_column_max_str)




#for ii,jj in csv_column.items():
#   max_str_len = 0
#   print(ii,jj)
#   print(max(len(ii), len(max(jj, key=len))))


   #for kk in range(0, len(jj)):
      #if (max_str_len < len(ii)):
      #   max_str_len = len(ii)
      #elif (max_str_len < len(jj[kk])):
      #   max_str_len = len(jj[kk])):
      #print(len(ii),len(jj[kk]))

#print(df)
#print()
#print(csv_df)

#print(csv_df.row_number, csv_df.column_number)
#print(rcf.read_csv_file_row_number("train.csv", ","))

#print(df.loc[:,"Pclass"])

#print(csv_df.data_array["Pclass"])

#df_columns = rcf.dataframe_columns(df)

#print(df_column)

outp.message_box_array(["test 1","test 2"], 100, header=["header 1", "header 2"], align="center")

outp.message_box_array(["test 1","test 2"], 100)


#print(csv_df)

